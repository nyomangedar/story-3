package id.ac.ui.cs.ddp2.project01;

import java.util.Scanner;

public class MyFirstClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number fo radius");
		double radius = input.nextDouble();
		
		double area = radius*3.14;
		
		System.out.println("The area for the radius of " + radius + " is " + area);
		
	}

}
