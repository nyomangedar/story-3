package ddp2.lab01.part3;

import java.util.Scanner;

public class Circle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final double PI = 3.14159;
		
		// Create the "scan" object to read from System.in
		Scanner scan= new Scanner(System.in);
		
		// Create a statement to ask the user to input a number for radius
		System.out.println("Please enter a value for the radius");
		
		// A read statement to that reads the value of "scan"
		double radius =scan.nextDouble();
		double area = PI * radius * radius;
		
		System.out.println("The area of a circle with radius " + radius + " is "+ area );
		
		// A custom variable to store the circumference value
		double circumference = 2 * PI * radius; 
		
		// Statement to give the user the circumference of the circle
		System.out.println("The circumference of a circle with radius " + radius + " is " + circumference);

	
		// Second Circle
		System.out.println("Please enter a value for the 2nd radius");
		radius =scan.nextDouble();
		area = PI * radius * radius;
		System.out.println("The area the 2nd circle with radius " + radius + " is "+ area );
		circumference = 2 * PI * radius;
		System.out.println("The circumference of the 2nd circle with radius " + radius + " is " + circumference);
	
	
	
	}

}
