package ddp2.lab02.part2;

public class StringConversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// String to INT
		String value1 ="12345";
		int intValue = Integer.parseInt(value1);
		System.out.println("intValue= "+ intValue);
		
		// String to Double
		String value2 = "12.345";
		double doubleValue = Double.parseDouble(value2);
		System.out.println("doubleValue= "+ doubleValue);
		
		// String to Long
		String value3 ="9876543210";
		long longValue = Long.parseLong(value3);
		System.out.println("longValue= "+ longValue);
		
		// String to Short
		String value4 = "321";
		short shortValue = Short.parseShort(value4);
		System.out.println("shortValue= "+ shortValue);
		
		// String to Byte
		String value5 ="-28";
		byte byteValue = Byte.parseByte(value5);
		System.out.println("byteValue= "+ byteValue);
		
		// String to Float
		String value6 ="-45.237";
		float floatValue = Float.parseFloat(value6);
		System.out.println("floatValue= "+ floatValue);
		
		// Hex to Int
		String value7 ="3BABE";
		int valueBABE = Integer.parseInt(value7, 16);
		System.out.println("valueBABE= "+ valueBABE);
		
		// Binary to INT
		String value8 ="11001";
		int valueBinary = Integer.parseInt(value8, 2);
		System.out.println("valueBinary= "+ valueBinary);
		

	}

}
